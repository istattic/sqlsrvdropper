<?php
/**
 * Created by PhpStorm.
 * User: zfowler
 * Date: 10/11/2017
 * Time: 11:49 AM
 */

namespace ISTAttic\DB;

use Illuminate\Support\Facades\DB;

class SqlSrvColumnDropper
{
    public static function prepSqlSrvDropContraint($table, $column)
    {
        $tableName = $table;
        if ('sqlsrv' == DB::connection()->getDriverName()) {
            $sql = "SELECT OBJECT_NAME([default_object_id]) AS name
                    FROM SYS.COLUMNS
                    WHERE [object_id] = OBJECT_ID('[dbo].[$tableName]') AND [name] = '$column'";
            $defaultContraint = DB::selectOne($sql);
            DB::statement("ALTER TABLE [dbo].[$tableName] DROP CONSTRAINT $defaultContraint->name");
        }
    }
}